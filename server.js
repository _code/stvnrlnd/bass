'use strict';

// ------ Require the packages
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// ------ Configure the application
var config = require('./server_resources/config');
var User = require('./server_resources/models');

var server = express();
var api = express.Router();

// mongoose.connect('mongodb://localhost:27017/bass');
mongoose.connect('mongodb://' + config.dbuser + ':' + config.dbpassword + '@' + config.dburl);

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

// ------ Build the routes
api.route('/')
    .get(function(req, res) {
        res.json({
            "message": "Hello, beautiful!"
        });
    });

api.route('/users')
    .post(function(req, res) {
        var user = new User();
        user.name = req.body.name;
        user.save(function(err) {
            if (err){
                res.send(err);
            } else {
                res.json({ message: 'User created!' });
            }
        });
    })
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err){
                res.send(err);
            } else {
                res.json(users);
            }
        });
    });

api.route('/users/:user_id')
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err){
                res.send(err);
            } else {
                res.json(user);
            }
        });
    })
    .put(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err){
                res.send(err);
            } else {
                user.name = req.body.name;
                user.save(function(err) {
                    if (err){
                        res.send(err);
                    } else {
                        res.json({ message: 'User updated!' });
                    }
                });
            }
        });
    })
    .delete(function(req, res) {
        User.remove({
            _id: req.params.user_id
        }, function(err, user) {
            if (err){
                res.send(err);
            } else {
                res.json({ message: 'Successfully deleted' });
            }
        });
    });

server.use('/api', api);

// ------ Serve the public
server.listen(config.port, function() {
    console.log("Running on port", config.port);
});