'use strict';

module.exports = {
    port: process.env.PORT || 8080,
    dbuser: process.env.DBUSER,
    dbpassword: process.env.DBPASSWORD,
    dburl: process.env.DBURL
}